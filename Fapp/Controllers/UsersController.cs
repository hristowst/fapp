﻿namespace Fapp.Controllers
{
    using Fapp.Models;
    using Fapp.Services.Data.Contracts;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUsersService usersService;
        public UsersController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet("[action]")]
        public IEnumerable<UserResponseModel> GetAllUsers()
        {
            var all = usersService.All();
            var responseUsers = all.Select(u => new UserResponseModel { FirstName = u.FirstName, LastName = u.LastName });
            return responseUsers;
        }
    }
}