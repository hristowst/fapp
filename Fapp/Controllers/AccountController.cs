﻿namespace Fapp.Controllers
{
    using Fapp.Data.Models;
    using Fapp.ViewModel;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    public class AccountController : Controller
    {
		private readonly ILogger<AccountController> _logger;
		private readonly SignInManager<FappUser> _signInManager;
		private readonly UserManager<FappUser> _userManager;

		public AccountController(ILogger<AccountController> logger,
		  SignInManager<FappUser> signInManager,
		  UserManager<FappUser> userManager)
		{
			_logger = logger;
			_signInManager = signInManager;
			_userManager = userManager;
		}

		public IActionResult Login()
		{
			if (this.User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Index", "App");
			}

			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Login(LoginViewModel model)
		{
			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(model.Username,
				  model.Password,
				  model.RememberMe,
				  false);

				if (result.Succeeded)
				{
					if (Request.Query.Keys.Contains("ReturnUrl"))
					{
						return Redirect(Request.Query["ReturnUrl"].First());
					}
					else
					{
						return RedirectToAction("Shop", "App");
					}
				}
			}

			ModelState.AddModelError("", "Failed to login");

			return View();
		}

        [HttpGet, Authorize]
        public async Task<IActionResult> Logout()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
            }

            return RedirectToAction("Index", "App");
        }
    }
}
