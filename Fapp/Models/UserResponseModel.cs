﻿namespace Fapp.Models
{
    public class UserResponseModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
